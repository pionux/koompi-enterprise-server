
##  Introduction to PionuxEnterprise

PionuxEnterprise is base on Pionux OS  an Active Directory compatible open source Linux server for small businesses and simple networks. The management console lets you manage users, share files, and configure DHCP and DNS. PionuxEnterprise utilizes a technology called Samba, which is an open source implementation of the Active Directory framework. Although Samba is not actually Active Directory, it is designed to provide the same services and is compatible with almost all Active Directory components which provide network management services, such as  
user authentication and computer management.

It is as a designed simple and easy to use system, here are the main features of PionuxEnterprise.

## Active Directory Domain Controller
```
+ Active Directory Compatible Domain with Samba 4
+ User Management
+ Central domain and directory management
+ Users, Security groups, Distribution lists, Contacts
+ Multiple Organization Units (OUs), Group Policy Objects (GPOs)
+ Users and Groups access and modification permissions (ACLs)
+ Integrated software: Samba
```

## How to Install Pionux Enterprise
```
Run command: /opt/pionux/Server/version_0.1/serversetup/setup
```

## Pionux Struture

![](https://lh3.googleusercontent.com/nKxXTE_t9b2gZBNeIF8tSlx67SkZmW-B3hAr-lOL8Kkk8FUqPRZvba0zR9Jgf-qFIfAxWXhQeqoT "struture")
## Permission

![](https://lh3.googleusercontent.com/_BKDgp9DdshRnMF2kqS-13UwkxY1qBUf993xHxmFB93MPVW4mhmzDbtZzHqZAc8RifRqC_QDBZZc "permission")
## User Management

After install you will see application on Desktop "User Management" . In User Management have :
```
1. User, Group ,OU : is for create user, group, ou , setting password and setting user .
2. Computer : is for show computer that joined in domain and add computer to domain.
3. Storage : is for show storage of computer .
4. Service : is for show all service is running.
```

![enter image description here](https://lh3.googleusercontent.com/t03ZRQz3vTYw-I1aQ1_XD_y6pGiVf9fN-82KzZyYrFNgwRQR705RCWcxgEfMijbBBFqdpRTakp71)

## Client Access
![enter image description here](https://lh3.googleusercontent.com/hv2AzQtvEE2y0x9-fF5KfX1jdvamlo6qQMkptw88ZE72MovZHrMumzLIuyHH_HP3ABaP_cl_4NqZ)
