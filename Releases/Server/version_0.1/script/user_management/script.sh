#!/bin/sh -e

####### color #######
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
####### update #######
sudo pacman -Syu --noconfirm

#----------------------- INSTALL -----------------------

####### NTP version 4.2.8.p13-2 #######
findntp=$(pacman -Qs ntp | grep ntp | awk -F" " '{print $1}')
if [ ${findntp} == "local/ntp" ] 
then
    echo
    echo -e "${YELLOW}ntp --> was installed.${NC}"
else 
   sudo pacman -S ntp --noconfirm
   echo -e "${YELLOW}ntp installed successful.${NC}"
fi

####### bind-tools version 9.14.4-1 #######
findbindtools=$(pacman -Qs bind-tools | grep local/bind-tools | awk -F" " 'NR==1{print $1}')
if [ ${findbindtools} == "local/bind-tools" ] 
then
    echo 
    echo -e "${YELLOW}bind-tools --> was installed.${NC}"
else 
   sudo pacman -S bind-tools --noconfirm
   echo -e "${YELLOW}bind-tools installed successful.${NC}"
fi

####### bind version 9.14.4-1 #######
findbind=$(pacman -Qs bind | grep local/bind | awk -F" " 'NR==1{print $1}')
if [ ${findbind} == "local/bind" ] 
then
    echo 
    echo -e "${YELLOW}bind --> was installed.${NC}"
else 
   sudo pacman -S bind --noconfirm
   echo -e "${YELLOW}bind installed successful.${NC}"
fi

####### pam-krb5 version 4.8-1 #######
findpamkrb5=$(pacman -Qs pam-krb5 | grep local/pam-krb5 | awk -F" " '{print $1}')
if [ ${findpamkrb5} == "local/pam-krb5" ] 
then
    echo 
    echo -e "${YELLOW}pam-krb5 --> was installed.${NC}"
else 
   sudo pacman -S pam-krb5 --noconfirm
   echo -e "${YELLOW}pam-krb5 installed successful.${NC}"
fi

####### samba version 4.10.6-1 #######
findsamba=$(pacman -Qs samba | grep local/samba | awk -F" " '{print $1}')
if [ ${findsamba} == "local/samba" ] 
then
    echo 
    echo -e "${YELLOW}samba --> was installed.${NC}"
else 
   sudo pacman -S samba --noconfirm
   echo -e "${YELLOW}samba installed successful.${NC}"
fi

####### openresolv version 3.9.1-1#######
findopenresolv=$(pacman -Qs openresolv | grep local/openresolv | awk -F" " '{print $1}')
if [ ${findopenresolv} == "local/openresolv" ] 
then
    echo 
    echo -e "${YELLOW}openresolv --> was installed.${NC}"
else 
   sudo pacman -S openresolv --noconfirm
   echo -e "${YELLOW}openresolv installed successful.${NC}"
fi

####### vsftpd version 3.0.3-6 #######
findvsftpd=$(pacman -Qs vsftpd | grep local/vsftpd | awk -F" " '{print $1}')
if [ ${findvsftpd} == "local/vsftpd" ] 
then
    echo 
    echo -e "${YELLOW}vsftpd --> was installed.${NC}"
else 
   sudo pacman -S vsftpd --noconfirm
   echo -e "${YELLOW}vsftpd installed  successful.${NC}"
fi

####### dhcp-4.4.1-4 #######
finddhcp=$(pacman -Qs dhcp | grep local/dhcp | awk -F" " 'NR==1{print $1}')
if [ ${finddhcp} == "local/dhcp" ] 
then
    echo 
    echo -e "${YELLOW}dhcp --> was installed.${NC}"
else 
   sudo pacman -S dhcp --noconfirm
   echo -e "${YELLOW}dhcp installed successful.${NC}"
fi

####### docker version 1:19.03.1-2 #######
finddocker=$(pacman -Qs docker | grep local/docker | awk -F" " 'NR==1{print $1}')
if [ ${finddocker} == "local/docker" ] 
then
    echo 
    echo -e "${YELLOW}docker --> was installed.${NC}"
else 
   sudo pacman -S docker --noconfirm
   echo -e "${YELLOW}docker installed successful.${NC}"
fi

####### docker-compose version 1:19.03.1-2 #######
finddockercompose=$(pacman -Qs docker-compose | grep local/docker-compose | awk -F" " 'NR==1{print $1}')
if [ ${finddockercompose} == "local/docker-compose" ] 
then
    echo 
    echo -e "${YELLOW}docker-compose --> was installed.${NC}"
else 
   sudo pacman -S docker-compose --noconfirm
   echo -e "${YELLOW}docker-compose installed successful.${NC}"
fi

#----------------------- CONFIGURATION -----------------------
######## SAMBA #######

sudo -S samba-tool domain provision --use-rfc2307 --interactive

######## BIND #######