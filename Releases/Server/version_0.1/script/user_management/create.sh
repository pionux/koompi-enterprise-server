#!/bin/bash
#This file was create on 22/03/19

#==================== function to add group ====================== 
function Group(){
    sudo samba-tool group addmembers $group $username > /dev/null 2>&1
    sudo samba-tool group addmembers network $username > /dev/null 2>&1
    sudo samba-tool group addmembers video $username > /dev/null 2>&1
    sudo samba-tool group addmembers storage $username > /dev/null 2>&1
    sudo samba-tool group addmembers lp $username > /dev/null 2>&1
    sudo samba-tool group addmembers audio $username > /dev/null 2>&1
    sudo samba-tool group addmembers wheel $username > /dev/null 2>&1
}
#==================== Function samba ======================
function samba(){
    file="/etc/samba/smb.conf"
    echo [$username] >> $file
    echo     \   path = /home/FileServer/$ou/$group/$username >> $file 
    echo     \   valid users = $username >> $file 
    echo     \   public = no >> $file 
    echo     \   writeable = yes >> $file
    echo     \   browseable = yes >> $file
    echo     \   create mask = 700 >> $file
    echo     \   directory mask = 700 >> $file
}
#==================== Function create folder ====================== 
function folder(){
    mkdir -p /home/FileServer/$ou/$group/$username
    chown -R $username:$group /home/FileServer/$ou/$group/$username
    chmod 770 /home/FileServer/$ou/$group/$username
}
#==================== function expiry ====================== 
function expiry(){
    if [[ "$Expiry" == "Disable" ]]
    then 
        sudo samba-tool user setexpiry $username  --noexpiry
    else
        sudo samba-tool user setexpiry --days=$Day $username  
    fi 

}

OLDIFS=$IFS
IFS="	"
while read username fistname surname passwd ou group userID groupID Expiry Day domain
do 
    echo -e "\e[1;33m =====> Create user $username <=====\e[0m\n\
    Username        : $username \n\
    Firstname       : $fistname \n\
    Surname         : $surname \n\
    Password        : "********" \n\
    OU              : $ou \n\
    Group           : $group \n\
    UserID          : $userID \n\
    GroupID         : $groupID \n\
    Expriy password : $Expiry \n\
    Day             : $Day  \n\
    Domain          : $domain \n"
#Create 
    finduser=$(sudo samba-tool user list | grep $username)
    if [[ $username == $finduser ]];then 
        printf "$username : already exists\n\n"
    else 
        findou=$(sudo samba-tool ou list | grep $ou | awk -F'=' '{print $2}')
        if [[ $findou != $ou ]];then
            printf "$ou :dose not already exists.\n\n"
            echo "Please create ou=$ou before add user."
            exit;
        else
            findgroup1=$(sudo samba-tool user list | grep $group)
            if [[ $findgroup1 == $group ]];then 
                echo "Name Group $group is the same name user."
                echo "Please change name group."
                exit
            else 
                findgroup=$(sudo samba-tool group list | grep $group)
                if [[ $findgroup != $group ]];then
                    echo "Can't not found group $group"
                    echo "Please create group $group before create user."
                    exit; 
                else 
                    sudo samba-tool user create $username $passwd \
                    --nis-domain=$domain   --given-name=$fistname --surname=$surname \
                    --unix-home=/home/$username --uid-number=$userID \
                    --login-shell=/bin/fales --gid-number=$groupID 
                    Group #call function group
                    echo "Add group successfully."
                    samba #call function samba
                    echo "Add samba successfully."
                    folder # call function folder
                    echo "create folder successfully."
                    expiry # call function Expiry
                    folder # call function folder
                    sudo samba-tool user setpassword $username --newpassword=$passwd --must-change-at-next-login  

                fi #end of findgroup    
            fi # end of findgroup with username
        fi #end of findou
    fi #end of finduser
done < $1
IFS=$OLDIFS

