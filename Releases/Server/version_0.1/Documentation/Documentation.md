# PIONUX Enterprise Server

PIONUX Enterprise Server is an Active Directory compatible open source Linux server for small businesses and simple networks. The management console lets you manage users, share files, and configure DHCP and DNS. PIONUX Enterprise Server utilizes a technology called Samba, which is an open source implementation of the Active Directory framework. Although Samba is not actually Active Directory, it is designed to provide the same services and is compatible with almost all Active Directory components which provide network management services, such as  
user authentication and computer management.

It is as a designed simple and easy to use system, here are the main features of PIONUX Enterprise Server.


### Infrastructure
```
+ DHCP and DNS server 
+ NTP server 
+ Certification Authority (CA) 
+ Virtual Private Networks (VPNs)
+ Instant Messaging (IM) service
+ FTP Server
+ Antivirus on-access scan
```
### Directory & Domain
```
+ Active Directory Compatible Domain with Samba 4
+ User Management
+ Computer Management
+ Backup System
+ Central domain and directory management
+ Users, Security groups, Distribution lists, Contacts
+ Multiple Organization Units (OUs), Group Policy Objects (GPOs)
+ Users and Groups access and modification permissions (ACLs)
+ Management of user profile pictures
+ Integrated software: Samba
```
# 
