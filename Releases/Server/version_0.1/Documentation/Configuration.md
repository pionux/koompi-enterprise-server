# Install packages
``` 
$ sudo pacman -S bind-tools
$ sudo pacman -S krb5
$ sudo pacman -S ntp 
$ sudo pacman -S openresolv
$ sudo pacman -S samba
$ sudo pacman -S bind
$ sudo pacman -S vsftpd
```

# FTP server
An FTP server is a computer which has a file transfer protocol (FTP) address and is dedicated to receiving an FTP connection.

**1. Install packages**
```
$ sudo nano /etc/vsftpd.conf

#line 12 : change like this ( login with local user)
anonymous_enable=NO
			
#line 15 : uncomment (allow local users to log in)
local_enable=YES
					
#line 18: uncomment(enable any form of FTP write command)
write_enable=YES
					
# add to the end : specify chroot directory
# if not specified, users' home directory equals FTP home directory
#Add
local_root=public_html
seccomp_sandbox=NO
```

+ Add Like This 
```
$ sudo nano /etc/pam.d/vsftpd

#%PAM-1.0
account    required  pam_listfile.so onerr=fail item=user sense=allow file=/etc/vsftpd.user_list
account    required  pam_unix.so
auth       required  pam_unix.so
```
**3. Start Service** 
```
$ sudo systemctl enable vsftpd
$ sudo systemctl start vsftpd
```
**4. Verify**
```
$ ftp <IP Address>
Connected to <IP Address>  
220 Welcome to blah FTP service.  
Name (<IP Address>:server): <Name>  
331 Please specify the password.  
Password:
230 Login successful.  
Remote system type is UNIX.  
Using binary mode to transfer files.  
ftp>
```
# NTP server
**NTP** ( **Network Time Protocol** ). keeps the system time synchronised on the different computers within a network.

**1. Install package ntp server**
```		
$ sudo pacman -S ntp
```
**2. configure timezone**
```	
$ sudo nano /etc/ntp.conf

#comment out default settings and add NTP Servers for your timezone line 10
#server 0.arch.pool.ntp.org  
#server 1.arch.pool.ntp.org  
#server 2.arch.pool.ntp.org  
#server 3.arch.pool.ntp.org
#add this comand
server 0.hk.pool.ntp.org
server 1.hk.pool.ntp.org
server 2.hk.pool.ntp.org
server 3.hk.pool.ntp.org 
```
**3. start service**
```
sudo systemctl enable ntpd
sudo systemctl start ntpd
sudo systemctl restart ntpd
```		
**4. Verify**
```
$ ntpq -p
remote				    refid		st t when 	poll reach	 delay	 offset	 jitter  
=================================================================================== 
150.109.127.233		119.28.230.190	3  u   7	 64		1	 37.960	 -2.330	  0.000  
time.hk.greenho		223.255.185.2	2  u   7	 64		1	 40.854	 -12.746  0.000  
ntp.hkg10.hk.le		130.133.1.10	2  u   4	 64		1	 40.588	 3.838	  0.000  
tick.hk.china.l		118.190.21.209	3  u   4	 64		1	 52.214	 3.510	  0.000
```
**5. Client**
```
$ sudo ntpdate 0.hk.pool.ntp.org
 16 May 17:54:44 ntpdate[16044]: adjust time server 17.253.84.251 offset 0.043274 sec	
```
# DNS server

**DNS** ( **Domain Name System** ) provides access to services and hosts using names instead of IP addresses, these are easier to memorise. Archlinux ships with BIND (Berkley Internet Naming Daemon), the most common program used for maintaining a name server on Linux.

**1. Install bind package.**
```
$ sudo pacman -S bind
```
**2. planing**
```	
-   Private network address is <Network/prefix>.
-   Private network name is DNS
-   IP address of DNS server for private network is <Network>. This DNS server uses	recursive query.
-   IP address of DNS server for internet is <IP Address>.
```
**3. configuration**  

This is the configration file for BIND.
Allow query from private network.
 Allow recursive query.
```
$ sudo nano /etc/named.conf
 options {
    directory "/var/named";
    pid-file "/run/named/named.pid";

    // Uncomment these to enable IPv6 connections support
    // IPv4 will still work:
    //  listen-on-v6 { any; };
    // Add this for no IPv4:
    //  listen-on { none; };

    auth-nxdomain yes;
    datasize default;
    empty-zones-enable no;
    tkey-gssapi-keytab "/var/lib/samba/private/dns.keytab";
    forwarders { <IP Address>; };

    //  Add any subnets or hosts you want to allow to use this DNS server (use "; " delimiter)
    allow-query     { <Network/prefix>; 127.0.0.0/8; };

    //  Add any subnets or hosts you want to allow to use recursive queries
    allow-recursion { <Network/prefix>; 127.0.0.0/8; };

    //  Add any subnets or hosts you want to allow dynamic updates from
       allow-update    { <Network/prefix>; 127.0.0.0/8; };

    allow-transfer { none; };
    version none;
    hostname none;
    server-id none;
    };

    zone "example.com" IN {
    type master;
    file "example.com.zone";
    };

    zone "1.168.192.in-addr.arpa" IN {
    type master;
    file "192.db";
    };

    zone "localhost" IN {
    type master;
    file "localhost.zone";
    };

    zone "0.0.127.in-addr.arpa" IN {
    type master;
    file "127.0.0.zone";
    };

    zone "1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" {
    type master;
    file "localhost.ip6.zone";
    };

    zone "255.in-addr.arpa" IN {
    type master;
    file "empty.zone";
    };

    zone "0.in-addr.arpa" IN {
    type master;
    file "empty0.zone";
    };

    zone "." IN {
    type hint;
    file "root.hint";
    };

    //Load AD integrated zones
    dlz "AD DNS Zones" {
    database "dlopen /usr/lib/samba/bind9/dlz_bind9_10.so";
    };

    //zone "example.org" IN {
    //    type slave;
    //    file "example.zone";
    //    masters {
    //        192.168.1.100;
    //    };
    //    allow-query { any; };
    //    allow-transfer { any; };
    //};
    //
    logging {
    channel xfer-log {
        file "/var/log/named.log";
            print-category yes;
            print-severity yes;
           severity info;
        };
        category xfer-in { xfer-log; };
        category xfer-out { xfer-log; };
        category notify { xfer-log; };
    };
```		
 + Set permissions:
``` 
# chgrp named /var/lib/samba/private/dns.keytab
# chmod g+r /var/lib/samba/private/dns.keytab
# touch /var/log/named.log
# chown root:named /var/log/named.log
# chmod 664 /var/log/named.log
```
+ Fix for recent versions of bind :
```
# copy /var/named/empty.zone /var/named/empty0.zone
# chown root:named /var/named/empty0.zone
```			
		
**4. Configure zone**
This is a zone file for private network.
DNS server hostname is server.
```					
$ sudo nano /var/named/koompi.local.zone
	
$TTL 86400  
@ IN SOA example.com. root.example.com (  
		2017062705  
		3600  
		900  
		604800  
		86400  
)  
@			IN		NS		example.  
example		IN		A		192.168.1.110 
```
**5. revase zone**
```
$ sudo nano /var/named/192.db
	
$TTL 604800  
@ IN SOA example. root.example. (  
		2017062705  
		3600  
		900  
		604800  
		86400  
)  
@			    IN		NS		example.  
example			IN		A		192.168.1.110  
			    IN		PTR		example.com.  
			    IN		A		192.168.1.110
110		    	IN		PTR		example.com.  
```
**6. start services**

Run bind
```
sudo systemctl enable named
sudo systemctl start named
sudo systemctl restart named
``` 
**8. Verify zone**
named-checkconf validates /etc/named.conf and included files.
```
$ sudo named-checkzone koompi.local /var/named/koompi.local.zone
  zone example.com/IN: loaded serial 2017062705  
  OK
$ sudo named-checkzone koompi.local /var/named/192.db
  zone example.com/IN: loaded serial 2017062705  
  OK
```
**9.   Try to resolv Name or Address normally.**
```
$ dig example.com.
	
 ; <<>> DiG 9.14.1 <<>> example.com  
 ;; global options: +cmd  
 ;; Got answer:  
 ;; WARNING: .local is reserved for Multicast DNS  
 ;; You are currently testing what happens when an mDNS query is leaked to DNS  
 ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7544  
 ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1  
	  
 ;; OPT PSEUDOSECTION:  
 ; EDNS: version: 0, flags:; udp: 4096  
 ; COOKIE: ba717babccd589eb3448c8b85cdd094c6eb0ff1841c96c42 (good)  
 ;; QUESTION SECTION:  
 ;koompi.local. IN A  
					  
 ;; ANSWER SECTION:  
example.com. 	86400		 IN 		A 		192.168.1.110 
					  
 ;; Query time: 0 msec  
 ;; SERVER: 192.168.1.110 #53(192.168.1.110)  
 ;; WHEN: ព្រ ៥ 16 13:55:08 +07 2019  
 ;; MSG SIZE rcvd: 85

```
```
$ dig -x 192.168.1.110
	
; <<>> DiG 9.14.1 <<>> -x 192.168.1.110  
;; global options: +cmd  
;; Got answer:  
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25853  
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1  
				  
;; OPT PSEUDOSECTION:  
; EDNS: version: 0, flags:; udp: 4096  
; COOKIE: a8b32c11e5c7ab4dea5c11335cdd09d78d90d4ebb28c8025 (good)  
;; QUESTION SECTION:  110.1.168.192.in-addr.arpa. IN PTR  
				  
;; ANSWER SECTION:  
110.1.168.192.in-addr.arpa. 	604800 		IN		PTR	example.com.  
				  
;; Query time: 0 msec  
;; SERVER: 192.168.1.222#53(192.168.1.222)  
;; WHEN: ព្រ ៥ 16 13:57:27 +07 2019  
;; MSG SIZE rcvd: 128
```
# DHCP server
**DHCP** ( **Dynamic Host Configuration Protocol** ) is a network server that automatically provides and assigns IP addresses, default gateways and other network parameters to client devices .

**1. Install dhcp package**
```
$ sudo pacman -S dhcp
```
**2. configure /etc/dhcpd.conf**
	   Define domain name, DHCP server IP address and gateway IP address .
```	
$ sudo nano /etc/dhcpd.conf
	
subnet 192.168.1.0 netmask 255.255.255.0 { 
  range 192.168.1.100 192.168.1.200;
  option subnet-mask 255.255.255.0;
  option routers 192.168.1.1;
  option domain-name "example.com";
  option domain-name-servers 192.168.1.110;
  option broadcast-address 192.168.1.255;
  default-lease-time 28800;
  max-lease-time 43200;
  authoritative;

on commit {
    set ClientIP = binary-to-ascii(10, 8, ".", leased-address);
    set ClientName = pick-first-value(option host-name, host-decl-name);
    execute("/usr/bin/dhcpd-update-samba-dns.sh", "add", ClientIP, ClientName);
  }

on release {
    set ClientIP = binary-to-ascii(10, 8, ".", leased-address);
    set ClientName = pick-first-value(option host-name, host-decl-name);
    execute("/usr/bin/dhcpd-update-samba-dns.sh", "delete", ClientIP, ClientName);
  }

on expiry {
    set ClientIP = binary-to-ascii(10, 8, ".", leased-address);
    set ClientName = pick-first-value(option host-name, host-decl-name);
    execute("/usr/bin/dhcpd-update-samba-dns.sh", "delete", ClientIP, ClientName);
  }
}
```
  **3. run dhcpd4**
  Run service dhcpd.
```  
$ systemctl start dhcpd4
$ systemctl restart dhcpd4
```
**4. Execution result**
Run the following command on client machine.
IP address is provided.
```			
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
   link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
   inet 127.0.0.1/8 scope host lo
   valid_lft forever preferred_lft forever inet6 ::1/128 scope host
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast
   state UP qlen 1000
   link/ether 52:54:00:c0:07:60 brd ff:ff:ff:ff:ff:ff
   inet  192.168.1.101/24  scope global ens3
   valid_lft forever preferred_lft forever
   inet6 fe80::5054:ff:fec0:760/64 scope link
   valid_lft forever preferred_lft forever
```
+ DNS servers are provided  
```	
$ sudo /etc/resolv.conf
domain example.com
nameserver 192.168.1.110	
```
# Domain Controller

**2. Provisioning**

The first step to creating an Active Directory domain is provisioning. This involves setting up the internal [LDAP](https://wiki.archlinux.org/index.php/LDAP "LDAP"), [Kerberos](https://wiki.archlinux.org/index.php/Kerberos "Kerberos"), and DNS servers and performing all of the basic configuration needed for the directory. If you have set up a directory server before, you are undoubtedly aware of the potential for errors in making these individual components work together as a single unit. The difficulty in doing so is the very reason that the Samba developers chose to provide internal versions of these programs. The server packages above were installed only for the client utilities. Provisioning is quite a bit easier with Samba. Just issue the following command:

```
$ sudo samba-tool domain provision --use-rfc2307 --interactive
  Realm: 
  Domain [SRV]:
  Server Role (dc, member, standalone) [dc]:
  DNS backend (SAMBA_INTERNAL, BIND9_FLATFILE, BIND9_DLZ, NONE) [SAMBA_INTERNAL]:
  DNS forwarder IP address (write 'none' to disable forwarding) [127.0.0.53]: 10.0.0.10
  Administrator password:
```

**3. NTPD**
Create a suitable NTP configuration for your network time server. See [Network Time Protocol daemon](https://wiki.archlinux.org/index.php/Network_Time_Protocol_daemon "Network Time Protocol daemon") for explanations of, and additional configuration options.
```    
$ sudo nano /etc/ntp.conf

# Please consider joining the pool:
#
#     http://www.pool.ntp.org/join.html
#
# For additional information see:
# - https://wiki.archlinux.org/index.php/Network_Time_Protocol_daemon
# - http://support.ntp.org/bin/view/Support/GettingStarted
# - the ntp.conf man page

# Associate to Arch's NTP pool
server 0.arch.pool.ntp.org
server 1.arch.pool.ntp.org
server 2.arch.pool.ntp.org
server 3.arch.pool.ntp.org

# Restrictions
restrict default kod limited nomodify notrap nopeer mssntp
restrict 127.0.0.1
restrict ::1
restrict 0.arch.pool.ntp.org mask 255.255.255.255 nomodify notrap nopeer noquery
restrict 1.arch.pool.ntp.org mask 255.255.255.255 nomodify notrap nopeer noquery
restrict 2.arch.pool.ntp.org mask 255.255.255.255 nomodify notrap nopeer noquery
restrict 3.arch.pool.ntp.org mask 255.255.255.255 nomodify notrap nopeer noquery

# Location of drift file
driftfile /var/lib/ntp/ntpd.drift

# Location of the update directory
ntpsigndsocket /var/lib/samba/ntp_signd/
```
Create the state directory and set permissions:
```
# install -d /var/lib/samba/ntp_signd
# chown root:ntp /var/lib/samba/ntp_signd
# chmod 0750 /var/lib/samba/ntp_signd
```
Enable service
```
$ sudo systemctl enable ntpd.service 
$ sudo systemctl start ntpd.service
```

**Kerberos client utilities**

The provisioning step above created a perfectly valid krb5.conf file for use with a Samba domain controller. Install it with the following commands:
```
# mv /etc/krb5.conf{,.default}
# cp /var/lib/samba/private/krb5.conf /etc
```
**DNS**

You will need to begin using the local DNS server now. Reconfigure resolvconf to use only localhost for DNS lookups. Create the  `/etc/resolv.conf.tail`  (do not forget to substitute  internal.domain.tld  with your internal domain):
```
$ sudo nano /etc/resolv.conf.tail

# Samba configuration
search example.com
# If using IPv6, uncomment the following line
#nameserver ::1
nameserver 192.168.1.110 
```
Set permissions and regenerate the new `/etc/resolv.conf` file:
```      
# chmod 644 /etc/resolv.conf.tail
# resolvconf -u 
```
**Samba**

Enable and start the `samba.service` unit. If you intend to use the LDB utilities, you will also need create the `/etc/profile.d/sambaldb.sh` file to set **LDB_MODULES_PATH**:
```
export LDB_MODULES_PATH="${LDB_MODULES_PATH}:/usr/lib/samba/ldb"
```
Set permissions on the file and source it:
```
# chmod 0755 /etc/profile.d/sambaldb.sh
# . /etc/profile.d/sambaldb.sh
#
```
**5. Testing the installation**
 
**DNS**

First, verify that DNS is working as expected. Execute the following commands substituting appropriate values for **internal.domain.com** and **server**:
``` 
# host -t SRV _ldap._tcp.example.com.
# host -t SRV _kerberos._udp.example.com.
# host -t A .example.com.
```
You should receive output similar to the following:
```
_ldap._tcp.internal.domain.com has SRV record 0 100 389 example.com.
_kerberos._udp.internal.domain.com has SRV record 0 100 88 example.com.
example.com has address 192.168.1.110
```
**NT authentication**

Next, verify that password authentication is working as expected:
```
# smbclient //localhost/netlogon -U Administrator -c 'ls'
```
You will be prompted for a password (the one you selected earlier), and will get a directory listing like the following:
```
Domain=[INTERNAL] OS=[Unix] Server=[Samba 4.1.2]
	.                                   D        0  Wed Nov 27 23:59:07 2013
	..                                  D        0  Wed Nov 27 23:59:12 2013

		50332 blocks of size 2097152. 47185 blocks available
```
**Kerberos**
Now verify that the KDC is working as expected. Be sure to replace INTERNAL.DOMAIN.COM and use upper case letters:
```
$ kinit administrator@EXAMPLE.COM
```
You should be prompted for a password and get output similar to the following:
```
Warning: Your password will expire in 41 days on Wed 08 Jan 2014 11:59:11 PM CST
```
Verify that you actually got a ticket:
```
$ klist
Ticket cache: FILE:/tmp/krb5cc_0
Default principal: administrator@EXAMPLE.COM

Valid starting       Expires              Service principal
11/28/2013 00:22:17  11/28/2013 10:22:17  	krbtgt/EXAMPLE.COM@EXAMPLE.COM
renew until 11/29/2013 00:22:14
```
As a final test, use smbclient with your recently acquired ticket. Replace **server** with the correct server name:
```
# smbclient //example/netlogon -k -c 'ls'
```
 **Additional configuration DNS**

You will also need to create a reverse lookup zone for each subnet in your environment in DNS. It is important that this is kept in Samba's DNS as opposed to BIND to allow for dynamic updates by cleints. For each subnet, create a reverse lookup zone with the following commands. Replace  **server**.**internal**.**domain**.**tld**  and  **xxx**.**xxx**.**xxx**  with appropriate values. For  **xxx**.**xxx**.**xxx**, use the first three octets of the subnet in reverse order (for example: 192.168.0.0/24 becomes 0.168.192):
```
# samba-tool dns zonecreate example.com 1.168.192.in-addr.arpa -U Administrator
```
Now, add a record for you server (if your server is multi-homed, add for each subnet) again substituting appropriate values as above.  **zzz**  will be replaced by the fourth octet of the IP for the server:
```
# samba-tool dns add example.com 1.168.192.in-addr.arpa 110 PTR example.com -U Administrator
```
Restart the  `samba`  service. If using BIND for DNS, restart the  `named`  service as well.

Finally, test the lookup. Replace  **xxx**.**xxx**.**xxx**.**xxx**  with the IP of your server:
```
# host -t PTR 192.168.1.110
```
You should get output similar to the following:
```
1.168.192.in-addr.arpa domain name pointer example.com.
```
**TLS**

TLS support is not enabled by default, however, a default certificate was created when the DC was brought up. With the release of Samba 4.3.8 and 4.2.2, unsecured LDAP binds are disabled by default, and you must configure TLS to use Samba as an authentication source (without reducing the security of your Samba installation). To use the default keys, append the following lines to the "**[global]**" section of the  `/etc/samba/smb.conf`  file:

tls enabled  = yes
tls keyfile  = tls/key.pem
tls certfile = tls/cert.pem
tls cafile   = tls/ca.pem

If a trusted certificate is needed, create a signing key and a certificate request (see  [OpenSSL](https://wiki.archlinux.org/index.php/OpenSSL "OpenSSL")  for detailed instructions). Get the request signed by your chosen certificate authority, and put into this directory. If your certificate authority also needs an intermediate certificate, concatenate the certs (server cert first, then intermediate) and leave  **tls cafile**  blank.

Restart  `samba`  for the changes to take effect.

+ winbind
```
winbind enum users = yes
winbind enum groups = yes
template shell = /bin/bash
template homedir = /home/%U
```